
# %%
import tensorflow as tf

import pandas as pd
import numpy as np
import os
import keras
import matplotlib.pyplot as plt
from keras.layers import Dense,GlobalAveragePooling2D
from keras.applications import MobileNet
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model, load_model
from keras.optimizers import Adam


# %%



base_model=MobileNet(weights='imagenet',include_top=False) #imports the mobilenet model and discards the last 1000 neuron layer.

x=base_model.output
x=GlobalAveragePooling2D()(x)
x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
x=Dense(1024,activation='relu')(x) #dense layer 2
x=Dense(512,activation='relu')(x) #dense layer 3

preds=Dense(1,activation='relu')(x)
# preds=Dense(250,activation='softmax')(x) #final layer with softmax activation


# %%



model=Model(inputs=base_model.input,outputs=preds)
#specify the inputs
#specify the outputs
#now a model has been created based on our architecture


# %%



for layer in model.layers[:20]:
    layer.trainable=False
for layer in model.layers[20:]:
    layer.trainable=True


# %%



train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies
test_datagen=ImageDataGenerator(preprocessing_function=preprocess_input)

# dir = './train/'
train_dir = '../../dataset/hist/train'
train_generator=train_datagen.flow_from_directory(train_dir, # this is where you specify the path to the main data folder
                                                 target_size=(224,224),
                                                 color_mode='rgb',
                                                 batch_size=32,
                                                 class_mode='binary',
                                                 shuffle=True)

test_dir = '../../dataset/hist/test'
test_generator=test_datagen.flow_from_directory(test_dir,
                                                target_size=(224,224),
                                                color_mode='rgb',
                                                batch_size=32,
                                                class_mode='binary',
                                                shuffle=True)


# %%

# optimizer = tf.keras.optimizers.RMSprop(0.001)

model.compile(optimizer='Adam',loss='mse',metrics=['mae', 'mse', 'accuracy'])
# Adam optimizer
# loss function will be categorical cross entropy
# evaluation metric will be accuracy

import os
from keras.callbacks import ModelCheckpoint, EarlyStopping
checkpoint = ModelCheckpoint(os.getcwd() + "/mobile.h5", monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
early = EarlyStopping(monitor='val_loss', min_delta=0, patience=20, verbose=1, mode='auto')


step_size_train=train_generator.n//train_generator.batch_size
if  os.path.isfile(os.getcwd() + "/mobile.h5") : model = load_model(os.getcwd() + "/mobile.h5")

# %%
# generator=train_generator
hist = model.fit_generator(generator=test_generator,
                   validation_data=test_generator,
                   validation_steps=10,
                   steps_per_epoch=step_size_train,
                   epochs=2,callbacks=[checkpoint,early])


# %%
loss, mae, mse, acc = model.evaluate(test_generator, verbose=1)


# %%
